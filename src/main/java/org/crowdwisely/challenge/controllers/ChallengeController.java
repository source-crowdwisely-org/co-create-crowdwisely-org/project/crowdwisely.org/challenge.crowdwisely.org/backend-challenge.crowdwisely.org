package org.crowdwisely.challenge.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.crowdwisely.challenge.domain.Challenge;
import org.crowdwisely.challenge.repositories.ChallengeRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChallengeController {

	private final ChallengeRepository challengeRepository;

	public ChallengeController(ChallengeRepository challengeRepository) {
		this.challengeRepository = challengeRepository;
	}

	@GetMapping("/challenges")
	public CollectionModel<Challenge> getAllChallenges() {
		CollectionModel<Challenge> collectionModel = CollectionModel.of(challengeRepository.findAll());
		collectionModel.add(linkTo(methodOn(ChallengeController.class).getAllChallenges()).withSelfRel());
		return collectionModel;
	}

	@RequestMapping(value="/challenges/challenge", params="name", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public CollectionModel<Challenge> getChallengeByName(@RequestParam("name") String nameChallenge) {
		CollectionModel<Challenge> collectionModel = CollectionModel.of(challengeRepository.findByNameChallenge(nameChallenge));

		collectionModel.add(linkTo(methodOn(ChallengeController.class).getChallengeByName(nameChallenge)).withSelfRel());
		return collectionModel;
	}

	@RequestMapping(value="/challenges/challenge", params="trigger", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public CollectionModel<Challenge> getChallengeByTrigger(@RequestParam("trigger") String describeTrigger) {
		CollectionModel<Challenge> collectionModel = CollectionModel.of(challengeRepository.findByDescribeTrigger(describeTrigger));

		collectionModel.add(linkTo(methodOn(ChallengeController.class).getChallengeByTrigger(describeTrigger)).withSelfRel());
		return collectionModel;
	}

}