package org.crowdwisely.challenge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.crowdwisely.challenge.domain.Challenge;
import org.crowdwisely.challenge.repositories.ChallengeRepository;

@SpringBootApplication
public class ChallengeCrowdwiselyOrgApplication {

	private static final Logger log = LoggerFactory.getLogger(ChallengeCrowdwiselyOrgApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ChallengeCrowdwiselyOrgApplication.class, args);
	}

	@Bean
	CommandLineRunner initDatabase(ChallengeRepository challengeRepository) {
  
	  return args -> {
		log.info("Preloading " + challengeRepository.save(new Challenge("First Challenge", "Stomped on bug")));
		log.info("Preloading " + challengeRepository.save(new Challenge("Second Challenge", "Hive stung me")));
		log.info("Preloading " + challengeRepository.save(new Challenge("Third", "Hate this OAS")));
		log.info("Preloading " + challengeRepository.save(new Challenge("Acturion", "Beepob Training!")));
		log.info("Preloading " + challengeRepository.save(new Challenge("First", "Duplicate!!")));
	  };
	}

}
