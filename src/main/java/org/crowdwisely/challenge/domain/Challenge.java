package org.crowdwisely.challenge.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@ToString(callSuper = true)
@Document(collection = "challenges")
public class Challenge {

	@Id
	public String id;
	private String nameChallenge;
	private String describeTrigger;

	public Challenge(String nameChallenge, String describeTrigger) {
		this.nameChallenge = nameChallenge;
		this.describeTrigger = describeTrigger;
	}

	@Override
	public String toString() {
	  return "Challenge [id=" + id + ", nameChallenge=" + nameChallenge + ", describeTrigger=" + describeTrigger + "]";
	}

}
