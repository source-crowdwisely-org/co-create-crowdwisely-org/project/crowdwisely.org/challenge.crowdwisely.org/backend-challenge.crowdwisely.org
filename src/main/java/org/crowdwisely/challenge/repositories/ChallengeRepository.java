package org.crowdwisely.challenge.repositories;

import java.util.List;

import org.crowdwisely.challenge.domain.Challenge;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChallengeRepository extends MongoRepository<Challenge, String> {

    List<Challenge> findByNameChallenge(String nameChallenge);

    List<Challenge> findByDescribeTrigger(String describeTrigger);
    
}
